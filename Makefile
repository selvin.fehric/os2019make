program: main.cpp hello.cpp factorial.cpp
	clang++ -std=c++14 -c main.cpp -o main.o
	clang++ -std=c++14 -c hello.cpp -o hello.o
	clang++ -std=c++14 -c factorial.cpp -o factorial.o
	clang++ -o program main.o hello.o factorial.o
clean: 
	rm -rf *.o
	rm -rf program
